package picross;

public interface ScrollListener {
    public void onScroll(int scrollAmt);
}
